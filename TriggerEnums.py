# Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration

__author__  = 'Javier Montejo'
__version__="$Revision: 2.0 $"
__doc__="Enumerations for trigger types and periods"

from enum import IntEnum
from enum import Flag, auto,IntFlag
class TriggerType(Flag):
    el_single  = auto()
    el_multi   = auto()
    mu_single  = auto()
    mu_multi   = auto()
    j_single   = auto()
    j_multi    = auto()
    bj_single  = auto()
    bj_multi   = auto()
    tau_single = auto()
    tau_multi  = auto()
    g_single   = auto()
    g_multi    = auto()
    xe         = auto()
    ht         = auto()
    mu_bphys   = auto()
    exotics    = auto()
    afp        = auto()

    el          = el_single | el_multi
    mu          = mu_single | mu_multi
    j           = j_single  | j_multi
    bj          = bj_single | bj_multi
    tau         = tau_single| tau_multi
    g           = g_single  | g_multi

    ALL         = el | mu | j | bj | tau | g | xe | ht | mu_bphys | exotics | afp
    UNDEFINED  = 0


class TriggerPeriod(IntFlag):
    y2015             = auto()
    y2016periodA      = auto()
    y2016periodBD3    = auto()
    y2016periodD4plus = auto()
    y2017periodB1     = auto()
    y2017periodB2B4   = auto()
    y2017periodB5B7   = auto()
    y2017periodB8     = auto()
    y2017periodC      = auto()
    y2017periodD1D5   = auto()
    y2017periodD6     = auto()
    y2017periodEF     = auto()
    y2017periodGHIK   = auto()
    y2017lowmu        = auto()
    y2018periodBE     = auto()
    y2018periodFI     = auto()
    y2018lowmu        = auto()
    y2018periodKQ     = auto()

    runNumber         = auto() #Can't get higher than this, enters the run number domain
    future1p8e34      = auto() 
    future2e34        = auto()

    y2017periodB      = y2017periodB1   | y2017periodB2B4 | y2017periodB5B7 | y2017periodB8
    y2017periodD      = y2017periodD1D5 | y2017periodD6
    y2017periodAll    = y2017periodB    | y2017periodC    | y2017periodD    | y2017periodEF | y2017periodGHIK #low-mu period is not considered 
    y2018             = y2018periodBE   | y2018periodFI   | y2018periodKQ  #low-mu period is not considered 
    y2017             = y2017periodAll
    y2016             = y2016periodA    | y2016periodBD3  | y2016periodD4plus
    future            = future1p8e34    | future2e34

    def isBasePeriod(self):
        return TriggerPeriod.isRunNumber(self) or all([self & x == 0 or self & x == self for x in TriggerPeriod])

    @classmethod
    def isRunNumber(cls, number):
        try:
            var = int(number)
        except ValueError:
            return False
        #return (number==0 or (number > TriggerPeriod.runNumber and number < TriggerPeriod.runNumber*2))
        if not(isinstance(number, int)):
            return False
        else:
            return(var==0 or var>TriggerPeriod.runNumber)
        
    @classmethod
    def basePeriods(cls):
        return [x for x in TriggerPeriod if x.isBasePeriod()]

    @classmethod
    def toName(cls, p):
        return p if TriggerPeriod.isRunNumber(p) else p.name

    @classmethod
    def fromName(cls, p):
        return p if TriggerPeriod.isRunNumber(p) else TriggerPeriod[p]

class LBexceptions:
    ''' List of LBs to be skipped. 
        Usually due to accidental prescales that will be taken into account in future GRLs or via reduced luminosity.
        Users can modify this if needed, but hopefully there is no need ;)
    '''
    # Run number: [(LB start, LB end), ...] both inclusive
    exceptions = {
       276262: [(72 , 75 )], #toroid off keys 
       281317: [(116, 118)], #toroid off keys 
       299055: [(650, 700)], #Toroid was going down, turned muons off
       301932: [(233, 234)], #Accidentaly moved to MuScan prescales
       302831: [(4  , 10 )], #toroid off keys 
       336506: [(212, 260)], #Regular muscan but the defect is not in sync with the switch of keys
       341294: [(137, 156)], #Standby keys
       355650: [(117, 117)], #Last LB of emittance scan
       357283: [(117, 117)], #Last LB of emittance scan
       359623: [(129, 129)], #Last LB of emittance scan
       }

class TriggerRenaming:
    ''' Pairs of triggers that have been renamed at some point
        The code will complete each other so that each contains
        luminosity of both
        Assumes that they are orthogonal, i.e. they haven't run both at the same time
    '''
    pairs = [
        ("HLT_mu20_mu8noL1_e9_lhvloose_nod0","HLT_e9_lhvloose_nod0_mu20_mu8noL1"),
        ("HLT_mu20_mu8noL1_e9_lhvloose_nod0_L1EM8VH_MU20","HLT_e9_lhvloose_nod0_mu20_mu8noL1_L1EM8VH_MU20"),
        ("HLT_mu20_mu8noL1_e9_lhvloose_nod0_L1EM7_MU20","HLT_e9_lhvloose_nod0_mu20_mu8noL1_L1EM7_MU20"),
    ]

if __name__ == "__main__":
    from AthenaCommon.Logging import logging
    log = logging.getLogger(__name__)
    log.info(TriggerPeriod.y2015.isBasePeriod() )
    log.info(TriggerPeriod.y2017.isBasePeriod() )
    log.info(TriggerPeriod.basePeriods() )
